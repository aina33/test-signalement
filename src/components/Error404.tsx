import {FC} from 'react'
import {Link} from 'react-router-dom'

export const Error404 : FC = () => {
    return (
        <div className='error d-flex flex-column justify-content-center align-items-center'>
            <h1>404<br />Not found</h1>
            <Link to='/error'>Retour à la liste des utilisateurs</Link>
        </div>
    )
}