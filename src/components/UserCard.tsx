import {FC} from 'react'
import {User} from "../core/_models";
import Card from "react-bootstrap/Card";
import {generatePath, useNavigate} from "react-router-dom";

type Props = {
    user:User
}

export const UserCard :FC<Props> = ({user}) => {
    const navigate = useNavigate();
    const navigateToUserDetails = (userId:string) => {
        const path = generatePath(":id", { id: userId })
        navigate(path);
    };

    return(
        <Card onClick={()=> navigateToUserDetails(user.id.toString())}>
            <Card.Img variant="top" src={user.avatar} />
            <Card.Body>
                <Card.Title>{user.first_name} {user.last_name}</Card.Title>
                <Card.Text>
                    {user.email}
                </Card.Text>
            </Card.Body>
        </Card>
    )
}