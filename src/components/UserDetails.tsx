import {FC} from 'react'
import {Navigate, useParams} from "react-router-dom";
import {useQuery} from "react-query";
import {GET_SINGLE_USER_URL, getUserById} from "../core/_requests";
import {UserCard} from "./UserCard";

export const UserDetails:FC = () => {
    const {id} = useParams()
    const {
        isFetching,
        data: response,
    } = useQuery(
        GET_SINGLE_USER_URL,
        () => {
            return (id && !Number.isNaN(parseInt(id))) ? getUserById(parseInt(id)) : null
        },
        {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
    )
   return (
       <div className='user-list d-flex justify-content-center'>
           {
               isFetching ? <div className='loading'>Chargement ...</div>
               : ( response ? <UserCard user={response} /> : <Navigate to='/error' replace={true}/>)
           }
        </div>
    )
}