import {FC} from 'react'
import {useQuery} from "react-query";
import {GET_RESOURCE_LIST_URL, getResources} from "../core/_requests";
import {ResourceCard} from "./ResourceCard";

export const Resources :FC = () => {
    const {
        isFetching,
        data: response,
    } = useQuery(
        GET_RESOURCE_LIST_URL,
        () => {
            return getResources()
        },
        {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
    )

    return <>
        {isFetching && <div className='loading'>Chargement ...</div>}
        {!isFetching && response && response.data && response.data.length ?
            (
                <ul className='resource-list d-flex justify-content-center'>
                    {response.data.map((resource) => <ResourceCard key={resource.id} resource={resource}/>)}
                </ul>
            ) :
            (<div className='not-found'>data not found</div>)
        }
    </>
}