import {FC} from 'react'
import {Resource} from "../core/_models";
import Card from "react-bootstrap/Card";
import {generatePath, useNavigate} from "react-router-dom";

type Props = {
    resource:Resource
}

export const ResourceCard :FC<Props> = ({resource}) => {
    const navigate = useNavigate();
    const navigateToUserDetails = (userId:string) => {
        const path = generatePath(":id", { id: userId })
        navigate(path);
    };

    return(
        <Card onClick={()=> navigateToUserDetails(resource.id.toString())} style={{borderLeft: `3px solid ${resource.color}`}}>
            <Card.Body>
                <Card.Title style={{color: `${resource.color}`}}>{resource.year}</Card.Title>
                <Card.Text>
                    {resource.name}({resource.pantone_value})
                </Card.Text>
            </Card.Body>
        </Card>
    )
}