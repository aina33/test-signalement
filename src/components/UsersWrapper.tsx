import {FC} from 'react'
import {Outlet} from 'react-router-dom'

export const UsersWrapper : FC = () => {
    return (
        <div className='p-lg-20px'>
            <Outlet />
        </div>
    )
}