import React, {FC} from 'react'
import {Link, useLocation} from "react-router-dom";

export const Menu :FC = () => {
    const {pathname} = useLocation()
    const isCurrent = (to:string) =>  {
        return pathname.includes(to)
    }
    return (
        <nav className='navbar navbar-expand'>
            <ul className='navbar-nav'>
                <li className='nav-item'>
                    <Link to='users' className={'nav-link ' + (isCurrent('/users') ? 'active' : '')}>
                        Liste des utilisateurs
                    </Link>
                </li>
                <li className='nav-item'>
                    <Link to='resources' className={'nav-link '+ (isCurrent('/resources') ? 'active' : '')}>
                    Liste des ressources
                    </Link>
                </li>
            </ul>
        </nav>
    )
}