import {FC} from 'react'
import {Navigate, useParams} from "react-router-dom";
import {useQuery} from "react-query";
import {GET_SINGLE_RESOURCE_URL, getResourceById} from "../core/_requests";
import {ResourceCard} from "./ResourceCard";

export const ResourceDetails :FC = () => {
    const {id} = useParams()
    const {
        isFetching,
        data: response,
    } = useQuery(
        GET_SINGLE_RESOURCE_URL,
        () => {
            return (id && !Number.isNaN(parseInt(id))) ? getResourceById(parseInt(id)) : null
        },
        {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
    )
    return (
        <div className='resource-list d-flex justify-content-center'>
            {
                isFetching ? <div className='loading'>Chargement ...</div>
                    : ( response ? <ResourceCard resource={response} /> : <Navigate to='/error' replace={true}/>)
            }
        </div>
    )
}