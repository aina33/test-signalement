import {FC} from 'react'
import {GET_USER_LIST_URL, getUsers} from "../core/_requests";
import {useQuery} from "react-query";
import {UserCard} from "./UserCard";

export const Users:FC = () => {
    const {
        isFetching,
        data: response,
    } = useQuery(
        GET_USER_LIST_URL,
        () => {
            return getUsers()
        },
        {cacheTime: 0, keepPreviousData: true, refetchOnWindowFocus: false}
    )

    return <>
        {isFetching && <div className='loading'>Chargement ...</div>}
        {!isFetching && response && response.data && response.data.length ?
            (
                <ul className='user-list d-flex justify-content-center'>
                {response.data.map((user) => <UserCard key={user.id} user={user}/>)}
                </ul>
            ) :
            (<div>data not found</div>)
        }
        </>
}