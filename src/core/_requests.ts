import {User, Response, Resource} from "./_models";
import axios, {AxiosResponse} from 'axios'

const API_BASE_URL = 'https://reqres.in/'
export const GET_USER_LIST_URL = `${API_BASE_URL}api/users`
export const GET_RESOURCE_LIST_URL = `${API_BASE_URL}api/unknown`
export const GET_SINGLE_USER_URL = `${API_BASE_URL}api/users/`
export const GET_SINGLE_RESOURCE_URL = `${API_BASE_URL}api/unknown/`

export type UsersQueryResponse = Response<Array<User>>
export type ResourcesQueryResponse = Response<Array<Resource>>

export const getUsers = (): Promise<UsersQueryResponse> => {
    return axios
        .get(`${GET_USER_LIST_URL}`)
        .then((d: AxiosResponse<UsersQueryResponse>) => d.data)
}

export const getUserById = (id: number): Promise<User | undefined> => {
    return axios
        .get(`${GET_SINGLE_USER_URL}`, {
            params: {
                id: id,
            },
        })
        .then((response: AxiosResponse<Response<User>>) => response.data)
        .then((response: Response<User>) => response.data)
}

export const getResources = (): Promise<ResourcesQueryResponse> => {
    return axios
        .get(`${GET_RESOURCE_LIST_URL}`)
        .then((d: AxiosResponse<ResourcesQueryResponse>) => d.data)
}

export const getResourceById = (id: number): Promise<Resource | undefined> => {
    return axios
        .get(`${GET_SINGLE_RESOURCE_URL}`, {
            params: {
                id: id,
            },
        })
        .then((response: AxiosResponse<Response<Resource>>) => response.data)
        .then((response: Response<Resource>) => response.data)
}