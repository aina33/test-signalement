export interface Response<T> {
    data?: T
    "page" ?: number,
    "per_page" ?: number,
    "total" ?: number,
    "total_pages" ?: number,
}

export type User = {
    "id": number
    "email": string
    "first_name": string
    "last_name": string
    "avatar": string
}

export type Resource = {
    "id": number
    "name": string
    "year": number
    "color": string
    "pantone_value": string
}