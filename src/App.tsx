import React, {useEffect} from 'react';
import {Outlet} from 'react-router-dom'
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.scss';
import {Menu} from "./components/Menu";

function App() {
    useEffect(
        () => {
            const REACT_APP_TITLE = process.env.REACT_APP_TITLE
            document.title = `${REACT_APP_TITLE}`
        },
        []
    )
    return (
        <div className="App">
          <header className="App-header">
              <Menu />
          </header>
            <Outlet />
        </div>
    );
}

export default App;
