import {Route, Routes} from 'react-router-dom'
import {Users} from "../components/Users";
import {UserDetails} from "../components/UserDetails";
import {UsersWrapper} from "../components/UsersWrapper";

export const UsersRoutes = () => {
    return (
        <Routes>
            <Route path='/' element={<UsersWrapper />} >
                <Route path='/:id' element={<UserDetails />} />
                <Route index element={<Users />} />
            </Route>
        </Routes>
    )
}