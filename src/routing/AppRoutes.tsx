import {FC} from 'react'
import {Routes, Route, BrowserRouter, Navigate} from 'react-router-dom'
import App from '../App'
import {Error404} from "../components/Error404";
import {UsersRoutes} from "./UsersRoutes";
import {ResourceRoutes} from "./ResourceRoutes";

export const AppRoutes: FC = () => {
    return (
        <BrowserRouter>
            <Routes>
                <Route element={<App />} >
                    <Route path='error' element={<Error404 />} />
                    <Route path='users/*' element={<UsersRoutes/>} />
                    <Route path='resources/*' element={<ResourceRoutes/>} />
                    <Route index element={<Navigate to='users' replace={true}/>} />
                    <Route path='*' element={<Navigate to='error' replace={true}/>} />
                </Route>
            </Routes>
        </BrowserRouter>
    )
}
