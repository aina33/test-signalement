import {Route, Routes} from 'react-router-dom'
import {UsersWrapper} from "../components/UsersWrapper"
import {Resources} from "../components/Resources"
import {ResourceDetails} from "../components/ResourceDetails"

export const ResourceRoutes = () => {
    return (
        <Routes>
            <Route path='/' element={<UsersWrapper />} >
                <Route path='/:id' element={<ResourceDetails />} />
                <Route index element={<Resources />} />
            </Route>
        </Routes>
    )
}